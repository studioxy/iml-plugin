=== Iml Shipping ===
Contributors: (this should be a list of wordpress.org userid's)
Donate link: https://example.com/
Tags: woocommerce, shipping
Requires at least: 4.5
Tested up to: 5.5.1
Stable tag: 0.1.0
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html



== Description ==

= Админка =

После активации плагина в меню появится пункт `Настройки > IML настройки`. Ссылка `wp-admin/options-general.php?page=iml-settings`. Необходимо ввести данные авторизации.

Настройка ключа яндекс карт явлется опциональной. Если поле оставить пустым, то список пунктов самовывоза будет отображатья в виде селекта.

Поле **тестовый аккаунт** - служит для отладки. Заказы при включенном тесте помечаются как тест. Рекомендуется после установки сделать 1-2 заказа тестово, чтобы убедиться в корректности настроек.

Поле **Подсказки для городов** - служит в качестве подсказок из справочника IML. У каждого города свой код в системе. По нему определяются пункты вывоза. По умолчанию включен.

Поле **Метод расчетов** имеет два возможных значения *Сервер IML* и *Таблица*. По умолчанию выбрана *Таблица*.


== Installation ==

1. Загрузите папку `iml-shipping` в папку `/wp-content/plugins/`
1. Активируйте плагин через пункт в меню `Плагины`
1. Авторизирутесь и настройте согласно инструкции и ваших потребностей.

== Frequently Asked Questions ==

= A question that someone might have =

An answer to that question.

= What about foo bar? =

Answer to foo bar dilemma.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.0 =
Первая версия плагина

== Arbitrary section ==

You may provide arbitrary sections, in the same format as the ones above.  This may be of use for extremely complicated
plugins where more information needs to be conveyed that doesn't fit into the categories of "description" or
"installation."  Arbitrary sections will be shown below the built-in sections outlined above.

== A brief Markdown Example ==

Ordered list:

1. Some feature
1. Another feature
1. Something else about the plugin

Unordered list:

* something
* something else
* third thing

Here's a link to [WordPress](https://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
Titles are optional, naturally.

[markdown syntax]: https://daringfireball.net/projects/markdown/syntax
            "Markdown is what the parser uses to process much of the readme file"

Markdown uses email style notation for blockquotes and I've been told:
> Asterisks for *emphasis*. Double it up  for **strong**.

`<?php code(); // goes in backticks ?>`
