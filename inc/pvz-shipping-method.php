<?php

add_action('woocommerce_shipping_init', 'iml_pvz_shipping_method');
function iml_pvz_shipping_method() {
	if (!class_exists('IML_PVZ_Shipping_Method')) {
		class IML_PVZ_Shipping_Method extends WC_Shipping_Method {
			public function __construct($instance_id = 0) {
				$this->id = 'iml_pvz';
				$this->method_title = __('PVZ Shipping', 'iml-shipping');
				$this->availability = 'including';
				$this->instance_id = $instance_id;
				$this->supports = array(
					'shipping-zones',
					'instance-settings',
					'instance-settings-modal',
				);
				$this->method_description = __('PVZ shipping method', 'iml-shipping');
				$this->init();
				$this->enabled = isset($this->settings['enabled']) ? $this->settings['enabled'] : 'yes';
				$this->title = $this->get_option('title');
				parent::__construct($instance_id);
			}

			function init() {
				$this->init_form_fields();
				$this->init_settings();
				add_action("woocommerce_update_options_shipping_{$this->id}", [$this, 'process_admin_options']);
			}

			function init_form_fields() {
				$this->instance_form_fields = [
					'enabled' => [
						'title' => __('Enable', 'iml-shipping'),
						'type' => 'checkbox',
						'description' => __('Enable this shipping method', 'iml-shipping'),
						'default' => 'yes',
					],
					'title' => array(
						'title'       => __( 'Title', 'iml-shipping' ),
						'type'        => 'text',
						'description' => __( 'Title to be displayed on site', 'iml-shipping' ),
						'default'     => __( 'PVZ shipping', 'iml-shipping' ),
						'desc_tip' => true
					),
				];
			}

			function calculate_shipping($package = []) {
				$cost_counter = get_option('cost_counter');
				if ($cost_counter === 'table') {
					$this->calculate_from_table($package);
				} else if ($cost_counter === 'api') {
					try {
						$city = $package['destination']['city'];
						$store_city = get_option('shop_iml_region');
						$post_data = [];
						isset($_REQUEST['post_data']) ? parse_str($_REQUEST['post_data'] ,$post_data) : '';
						$pvz_address = isset($post_data['pvz_address']) ? $post_data['pvz_address'] : null;
						$cost = 0;

						$data['price'] = $package['contents_cost'];
						$data['pvz'] = $pvz_address;

						$weight = 0;
						foreach ($package['contents'] as $product) {
							$weight += (float) $product['data']->get_weight();
						}

						if ($weight > 0) {
							$data['Weight'] = $weight;
						}
						$deliveryPrice = IMLShippingApi::calculateCost($package, $this->id);
						if (isset($deliveryPrice['Price'])) {
							$cost += $deliveryPrice['Price'];
						}
						$rate = [
							'id' => $this->id,
							'label' => $this->title,
							'cost' => $cost,
							'package' => $package,
						];
						$this->add_rate($rate);
					} catch (Exception $e) {
						$this->calculate_from_table($shipping);
					}

				}
			}

			public function calculate_from_table($package) {
				$city = $package['destination']['city'];
				$store_city = get_option('shop_iml_region');
				$rate_option = '';
				if ($city === $store_city) {
					$rate_option = 'pvz_my_region';
				} else {
					$rate_option = 'pvz_other_regions';
				}
				$cost = get_option($rate_option);
				$cost = IMLHelper::additional_value($cost);
				$rate = [
					'id' => $this->id,
					'label' => $this->title,
					'cost' => $cost,
					'package' => $package,
				];
				$this->add_rate($rate);
			}
		}
	}
}

add_filter('woocommerce_shipping_methods', 'add_iml_pvz_shipping_method');
function add_iml_pvz_shipping_method($methods) {
	$methods['iml_pvz'] = 'IML_PVZ_Shipping_Method';
	return $methods;
}

