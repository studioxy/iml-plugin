<?php

class IMLShippingApi {
	public static function getAuthData() {
		return [
			'http_user' => get_option('iml_http_user'),
			'http_password' => get_option('iml_http_password'),
			'is_iml_test' => get_option('is_iml_test')['checked'] === '1' ? true : false,
		];
	}

	public static function getPrice($data, $type = 'pvz') {
		$content = [
			'Price' => $data['price'],
			'Job' => '24',
			'Weigth' => '1',
			'Volume' => 1,
			//'RegionTo' => 'МОСКВА',
			'RegionFrom' => 'МОСКВА',
		];

		if ($data['price'] === 0) {
			$content['Job'] = '24';
		} else {
			$content['Job'] = '24КО';
		}

		if ($type === 'pvz') {
			$content['specialCode'] = $data['pvz'];
			$content['RegionTo'] = self::getSD($data['pvz'])['RegionCode'];
		}

		if (isset($data['Weight'])) {
			$content['Weight'] = $data['Weight'];
		}
		$response = self::sendRequest(IMLHelper::$calculateLink, 'POST', $content);
		return $response;
	}

	public static function getDeliveryStatusesList() {
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = "{$prefix}_iml_delivery_status";
		return $wpdb->get_results("SELECT * FROM $table_name", ARRAY_A);
	}

	public static function getOrderStatusesList() {
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = "{$prefix}_iml_order_status";
		return $wpdb->get_results("SELECT * FROM $table_name", ARRAY_A);
	}

	public static function getRegionsList($like = null) {
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = "{$prefix}iml_regions";
		$query = "SELECT * FROM $table_name";
		if ($like) {
			$like = esc_sql($like);
			$query .= " WHERE Description LIKE '%$like%'";
		}
		return $wpdb->get_results($query, ARRAY_A);
	}

	public static function getSDList($regionCode = null) {
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = "{$prefix}iml_sd";
		$query = "SELECT * FROM $table_name";

		$where = '';
		if ($regionCode) {
			$where .= " WHERE RegionCode = '$regionCode' ";
		}

		$query .= $where;

		return $wpdb->get_results($query, ARRAY_A);
	}

	public static function getServicesList() {
		global $wpdb;
		$prefix = $wpdb->prefix;
		$table_name = "{$prefix}_iml_service";
		return $wpdb->get_results("SELECT * FROM $table_name", ARRAY_A);
	}

	public static function sendRequest($url, $method = 'GET', $content = []) {
		$credentials = self::getAuthData();
		$login = $credentials['http_user'];
		$password = $credentials['http_password'];
		$curl = curl_init($url);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		if ($method === 'POST') {
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($content));
		}
		curl_setopt($curl, CURLOPT_USERPWD, $login.":".$password);
		curl_setopt($curl, CURLOPT_SSLVERSION, 3);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		$response = curl_exec($curl);
		if (curl_errno($curl)) {
			return curl_error($curl);
		}
		curl_close($curl);
		$result = json_decode($response, true);
		return $result;
	}

	public static function parseSD() {
		$url = 'http://list.iml.ru/sd';
		$result = self::sendRequest($url);
		global $wpdb;
		$table_name = "{$wpdb->prefix}iml_sd";
		$query = "INSERT INTO $table_name (ID, CalendarWorkCode, Code, Name, RequestCode, RegionCode, SDIndex, Address, Phone, Email, FittingRoom, PaymentCard, PaymentPossible, ReceiptOrder, Latitude, Longitude, Homepage, ClosingDate, OpeningDate, CouponReceipt, DaysFreeStorage, SubAgent, DeliveryTimeFrom, DeliveryTimeTo, ReplicationPath, Submission, Special_Code, HowToGet, FormPostCode, FormRegion, FormCity, FormStreet, FormHouse, FormBuilding, FormOffice, FormKLADRCode, FormFIASCode, FormalizedArea, FormalizedLocality, Scale, TimeZone, Type) VALUES ";
		foreach ($result as $key => $item) {
			$item = esc_sql($item);
			$value = "
 ('{$item['ID']}', '{$item['CalendarWorkCode']}', '{$item['Code']}', '{$item['Name']}', '{$item['RequestCode']}', '{$item['RegionCode']}', '{$item['SDIndex']}', '{$item['Address']}', '{$item['Phone']}', '{$item['Email']}', '{$item['FittingRoom']}', '{$item['PaymentCard']}', '{$item['PaymentPossible']}', '{$item['ReceiptOrder']}', '{$item['Latitude']}', '{$item['Longitude']}', '{$item['Homepage']}', '{$item['ClosingDate']}', '{$item['OpeningDate']}', '{$item['CouponReceipt']}', '{$item['DaysFreeStorage']}', '{$item['SubAgent']}', '{$item['DeliveryTimeFrom']}', '{$item['DeliveryTimeTo']}', '{$item['ReplicationPath']}', '{$item['Submission']}', '{$item['Special_Code']}', '{$item['HowToGet']}', '{$item['FormPostCode']}', '{$item['FormRegion']}', '{$item['FormCity']}', '{$item['FormStreet']}', '{$item['FormHouse']}', '{$item['FormBuilding']}', '{$item['FormOffice']}', '{$item['FormKLADRCode']}', '{$item['FormFIASCode']}', '{$item['FormalizedArea']}', '{$item['FormalizedLocality']}', '{$item['Scale']}', '{$item['TimeZone']}', '{$item['Type']}')";
			if ($key < count($result) - 1) {
				$value .= ', ';
			}
			$query .= "$value ";
		}
		$query .= " ON DUPLICATE KEY UPDATE
			CalendarWorkCode = VALUES(CalendarWorkCode),
			Code=VALUES(Code),
			RequestCode=VALUES(RequestCode),
			RegionCode=VALUES(RegionCode),
			SDIndex=VALUES(SDIndex),
			Address=VALUES(Address),
			Phone=VALUES(Phone),
			Email=VALUES(Email),
			FittingRoom=VALUES(FittingRoom),
			PaymentPossible=VALUES(PaymentCard),
			PaymentPossible=VALUES(PaymentPossible),
			ReceiptOrder=VALUES(ReceiptOrder),
			Latitude=VALUES(Latitude),
			Longitude=VALUES(Longitude),
			Homepage=VALUES(Homepage),
			ClosingDate=VALUES(ClosingDate),
			OpeningDate=VALUES(OpeningDate),
			CouponReceipt=VALUES(CouponReceipt),
			DaysFreeStorage=VALUES(DaysFreeStorage),
			SubAgent=VALUES(SubAgent),
			DeliveryTimeFrom=VALUES(DeliveryTimeFrom),
			DeliveryTimeTo=VALUES(DeliveryTimeTo),
			ReplicationPath=VALUES(ReplicationPath),
			Submission=VALUES(Submission),
			Special_Code=VALUES(Special_Code),
			HowToGet=VALUES(HowToGet),
			FormPostCode=VALUES(FormPostCode),
			FormRegion=VALUES(FormRegion),
			FormCity=VALUES(FormCity),
			FormStreet=VALUES(FormStreet),
			FormHouse=VALUES(FormHouse),
			FormBuilding=VALUES(FormBuilding),
			FormOffice=VALUES(FormOffice),
			FormKLADRCode=VALUES(FormKLADRCode),
			FormFIASCode=VALUES(FormFIASCode),
			FormalizedArea=VALUES(FormalizedArea),
			FormalizedLocality=VALUES(FormalizedLocality),
			Scale=VALUES(Scale),
			TimeZone=VALUES(TimeZone),
			Type=VALUES(Type)
			";
		try {
			$wpdb->query($query);
		} catch (Exception $e) {
			var_dump($e);
		}
	}

	public static function parseDeliveryStatusesList() {
		$url = 'http://api.iml.ru/list/deliverystatus';
		$result = self::sendRequest($url);
		global $wpdb;
		$table_name = "{$wpdb->prefix}iml_delivery_status";
		$query = "INSERT INTO $table_name (Code, Description) VALUES ";
		foreach ($result as $key => $item) {
			$value = " ('{$item['Code']}', '{$item['Description']}')";
			if ($key < count($result) - 1) {
				$value .= ', ';
			}
			$query .= "$value ";
		}
		$query .= " ON DUPLICATE KEY UPDATE Code=VALUES(Code), Description=VALUES(Description)";
		try {
			$wpdb->query($query);
		} catch (Exception $e) {
			var_dump($e);
		}
	}

	public static function parseOrderStatusesList() {
		$url = 'http://api.iml.ru/list/orderstatus';
		$result = self::sendRequest($url);
		global $wpdb;
		$table_name = "{$wpdb->prefix}iml_order_status";
		$query = "INSERT INTO $table_name (Code, Description) VALUES ";
		foreach ($result as $key => $item) {
			$value = " ('{$item['Code']}', '{$item['Description']}')";
			if ($key < count($result) - 1) {
				$value .= ', ';
			}
			$query .= "$value ";
		}
		$query .= " ON DUPLICATE KEY UPDATE Code=VALUES(Code), Description=VALUES(Description)";
		try {
			$wpdb->query($query);
		} catch (Exception $e) {
			var_dump($e);
		}
	}

	public static function parseRegionsList() {
		$url = 'http://api.iml.ru/list/region';
		$result = self::sendRequest($url);
		global $wpdb;
		$table_name = "{$wpdb->prefix}iml_regions";
		$query = "INSERT INTO $table_name (Code, Description) VALUES ";
		foreach ($result as $key => $item) {
			$value = " ('{$item['Code']}', '{$item['Description']}')";
			if ($key < count($result) - 1) {
				$value .= ', ';
			}
			$query .= "$value ";
		}
		$query .= " ON DUPLICATE KEY UPDATE Code=VALUES(Code), Description=VALUES(Description)";
		try {
			$wpdb->query($query);
		} catch (Exception $e) {
			var_dump($e);
		}
	}

	public static function parseServicesList() {
		$url = 'http://api.iml.ru/list/service';
		$result = self::sendRequest($url);
		global $wpdb;
		$table_name = "{$wpdb->prefix}iml_service";
		$query = "INSERT INTO $table_name (Code, Description) VALUES ";
		foreach ($result as $key => $item) {
			$value = " ('{$item['Code']}', '{$item['Description']}')";
			if ($key < count($result) - 1) {
				$value .= ', ';
			}
			$query .= "$value ";
		}
		$query .= " ON DUPLICATE KEY UPDATE Code=VALUES(Code), Description=VALUES(Description)";
		try {
			$wpdb->query($query);
		} catch (Exception $e) {
			var_dump($e);
		}
	}

	public static function getServiceCode($shipping, $payment) {
		if ($shipping === 'iml_courier_after' && $payment === 'cod') {
			return '24НАЛ';
		}

		if ($shipping === 'iml_courier_after') {
			return '24';
		}

		if ($shipping === 'iml_courier') {
			return '24';
		}
	}

	public function createOrder($order, $forceTest = false) {
		$orderData = $order->get_data();
		$shipping = reset($order->get_items('shipping'))->get_method_id();
		$payment = $order->get_payment_method();

		$service = self::getServiceCode($shipping, $payment);
		if (!$service) {
			return;
		}

		$shop_iml_region = get_option('shop_iml_region');
		$contact = "{$orderData['shipping']['first_name']} {$orderData['shipping']['last_name']}";
		$phone = $orderData['billing']['phone'];
		$shipping_address = "{$orderData['shipping']['postcode']}, {$orderData['shipping']['country']} {$orderData['shipping']['state']} {$orderData['shipping']['city']} {$orderData['shipping']['address_1']} {$orderData['shiping']['address_2']}";
		$total = (int)$orderData['total'] - (int)$orderData['shipping_total'];
		$content =array(
			'CustomerOrder' => $order->get_id(),
			'Job' => '24',
			'BarCode' => '',
			'Contact' => $contact,
			'RegionCodeTo' => $orderData['shipping']['city'],
			'RegionCodeFrom' => $shop_iml_region,
			'Address' => $shipping_address,
			'PostCode' => $orderData['shipping']['postcode'],
			//'DeliveryPoint'=> '1',
			'Phone'=> $phone,
			//'Amount' => 0,
			//'Amount' => (float)$total,
			//'ValuatedAmount' => (float)$total,
			'Volume' => '1',
			'Comment' => $order->customer_message,
			'GoodItems' => [],
		);

		if ($shipping === 'iml_courier') {
			$content['Amount'] = (float)$total;
		}

		$totalWeight = 0;
		foreach ($order->get_items() as $product) {
			$productData = $product->get_data();
			$id = $productData['product_id'];
			$_product = wc_get_product($id);
			$weight = $_product->get_weight();

			if ($weight) {
				$totalWeight += $weight;
			}

			$goodItem = array(
				'productNo'=> $id,
				'productName' => $_product->get_name(),
				'productBarCode' => $_product->get_sku(),
				//'couponCode' => '10000002',
				//'discount' => '0',
				'weightLine' => $weight,
				//'amountLine' => $_product->get_price(),
				'statisticalValueLine' => $_product->get_price(),
				'deliveryService' => FALSE
			);
			if ($productData['variation_id']) {
				$goodItem['productVariant'] = $productData['variation_id'];
			}
			$content['GoodItems'][] = $goodItem;
		}
		$content['Weight'] = $totalWeight;

		if ($forceTest) {
			$content['Test'] = 'True';
		}
		//dump($content);

		return self::sendRequest(IMLHelper::$orderCreateLink, 'POST', $content);
	}

	public static function calculateServiceAndAmount($package, $method, $payment, $total) {
		$Job = null;
		$Price = 0;
		if ($method === 'iml_courier_after') {
			$Job = '24КО';
			$Price = $total;
		} else if ($method === 'iml_courier') {
			$Job = '24';
		} else if ($method === 'iml_pvz') {
			$Job = 'С24';
		} else if ($method === 'iml_pvz_after') {
			$Job = 'С24КО';
			$Price = $total;
		}

		$data = [
			'Job' => $Job,
			'Price' => $Price,
			'VualetedAmount' => $Price,
		];
		return $data;
	}

	/**
	 * Calculate Cost
	 */
	public static function calculateCost($package, $method = 'iml_courier', $payment = 'cod') {
		$weight = 0;
		$height = 0;
		$width = 0;
		$length = 0;
		$price = 0;

		$city = $package['destination']['city'];
		$shop_iml_region = get_option('shop_iml_region', true);
		$current_unit = strtolower(get_option('woocommerce_weight_unit'));

		$current_unit = strtolower(get_option('woocommerce_weight_unit'));
		$weight_c = 1;
		$dimension_c = 1;
		$dimension_unit = strtolower(get_option('woocommerce_dimension_unit'));

		$items = [];

		switch($dimension_unit){
		case 'm':
			$dimension_c = 100;
			break;
		case 'mm':
			$dimension_c = 0.1;
			break;
		}

		foreach ($package['contents'] as $cart_item) {
			$_product = $cart_item['data'];

			$productWeight = (float) $_product->get_weight() * $weight_c;
			$weight += $productWeight;

			$productHeight = (float) $_product->get_height() * $dimension_c;
			$height += $productHeight;

			$productWidth += (float) $_product->get_width() * $dimension_c;
			$width += $productWidth;

			$productLength = (float) $_product->get_length() * $dimension_c;
			$length += $productLength;

			$productPrice = (float) $_product->get_price();
			$price += $productPrice;

			$item = [
				'productNo' => $_product->get_id(),
				'productName' => $_product->get_name(),
				'weightLine' => $productWeight,
				'amountLine' => $productPrice,
				'itemQuantity' => $cart_item['quantity'],
				'statisticalValueLine' => $productPrice,
				'Length' => $productLength,
				'Height' => $productHeight,
				'Width' => $productWidth,
			];
			$items[] = $item;
		}

		if (!$city) {
			$city = $shop_iml_region;
			$error_message = __("Please select city  for a more accurate shipping calculation");
			if (!wc_has_notice($error_message, 'error') && defined('DOING_AJAX') && DOING_AJAX) {
				wc_add_notice($error_message, 'error');
			}
		}

		$weight = (string)max($weight, 0.1);
		$request = [
			'RegionFrom' => $shop_iml_region,
			'RegionTo' => $city,
			'Weigth' => max(0.1, $weight),
			'Volume' => 1,
			'Width' => max(0.1, $width),
			'Height' => max(0.1, $height),
			'Length' => max(0.1, $length),
			//'GoodItems' => $items,
		];

		$jobAndAmount = self::calculateServiceAndAmount($package, $method, $amount, $price);
		$request = array_merge($jobAndAmount, $request);

		$response = self::sendRequest(IMLHelper::$calculateLink, 'POST', $request);
		$data = [
			'city' => $city,
			'price' => null,
		];
		if (isset($response['Price'])) {
			$data['price'] = $response['Price'];
		} else {
			$messages = [];
			foreach ($response as $item) {
				$messages[] = $item['Mess'];
			}
			add_filter('woocommerce_package_rates', function ($rates) use ($method) {
				//unset($rates[$method]);
				return $rates;
			});
		}
		return $data;
	}

	public static function getSD($ID) {
		if (!$ID) return false;
		global $wpdb;
		$table_name = $wpdb->prefix . "iml_sd";
		$query = "SELECT * FROM $table_name WHERE ID=$ID";
		return $wpdb->get_row($query, ARRAY_A);
	}

	public static function getSDByRegion($RegionCode) {
		global $wpdb;
		$table_name = "{$wpdb->prefix}iml_sd";
		$query = "SELECT * FROM $table_name WHERE RegionCode='$RegionCode'";
		return $wpdb->get_results($query, ARRAY_A);
	}


	public static function getOrderData($order_id) {
		$url = IMLHelper::$orderStatus;
		$request = [
			'CustomerOrder' => $order_id,
		];
		$response = self::sendRequest($url, 'POST', $request);
		return $response;
	}
}
