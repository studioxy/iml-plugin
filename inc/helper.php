<?php

class IMLHelper {
	public static $deliveryStatusListLink = 'http://api.iml.ru/list/deliverystatus';
	public static $orderStatusListLink = 'http://api.iml.ru/list/orderstatus';
	public static $regionStatusListLink = 'http://api.iml.ru/region';
	public static $sdListLink = 'http://api.iml.ru/list/sd';
	public static $serviceListLink = 'http://api.iml.ru/list/service';

	public static $orderCreateLink = 'http://api.iml.ru/Json/CreateOrder';

	public static $calculateLink = 'http://api.iml.ru/GetPrice';

	public static $orderStatus = 'http://api.iml.ru/Json/GetStatuses';

	public static function checkAuth() {
		$login = get_option('iml_http_user');
		$pass = get_option('iml_http_password');
		if (!$login || !$pass) {
			return false;
		}
		return true;
	}

	public static function deleteTables() {
		$tables = [
			'iml_sd',
			'iml_regions',
			'iml_service',
			'iml_order_status',
			'iml_delivery_status',
		];
		global $wpdb;
		foreach ($tables as $table) {
			$table_name = $wpdb->prefix . $table;
			$wpdb->query("DROP TABLE IF EXISTS $table_name");
		}
	}

	public static function createTables() {
		global $wpdb;
		$prefix = $wpdb->prefix;
		$charset_collate = $wpdb->get_charset_collate();
		$lists_queries = [
			"CREATE TABLE IF NOT EXISTS {$prefix}iml_delivery_status (
				Code INT NOT NULL AUTO_INCREMENT,
				Description TEXT,
				PRIMARY KEY (Code)
			) $charset_collate",
			"CREATE TABLE IF NOT EXISTS {$prefix}iml_service (
				Code VARCHAR(256) NOT NULL,
				Description TEXT,
				PRIMARY KEY (Code)
			) $charset_collate",
			"CREATE TABLE IF NOT EXISTS {$prefix}iml_order_status (
				Code INT NOT NULL AUTO_INCREMENT,
				Description TEXT,
				PRIMARY KEY (Code)
			) $charset_collate",
			"CREATE TABLE IF NOT EXISTS {$prefix}iml_regions (
				Code VARCHAR(256) NOT NULL,
				Description TEXT,
				PRIMARY KEY (Code)
			) $charset_collate",
			"CREATE TABLE IF NOT EXISTS {$prefix}iml_sd (
				ID BIGINT NOT NULL AUTO_INCREMENT,
				CalendarWorkCode TEXT,
				Code VARCHAR(128),
				Name VARCHAR(256),
				RequestCode INT,
				RegionCode VARCHAR(256),
				SDIndex VARCHAR(256),
				Address TEXT,
				Phone VARCHAR(256),
				Email VARCHAR(256),
				FittingRoom VARCHAR(256),
				PaymentCard VARCHAR(128),
				PaymentPossible TINYINT,
				ReceiptOrder VARCHAR(128),
				Latitude DECIMAL(10, 6),
				Longitude DECIMAL(10, 6),
				Homepage VARCHAR(256),
				ClosingDate DATETIME,
				OpeningDate DATETIME,
				CouponReceipt VARCHAR(256),
				DaysFreeStorage INT,
				SubAgent INT,
				DeliveryTimeFrom VARCHAR(256),
				DeliveryTimeTo VARCHAR(256),
				ReplicationPath VARCHAR(256),
				Submission VARCHAR(256),
				Special_Code VARCHAR(256),
				HowToGet TEXT,
				FormPostCode VARCHAR(256),
				FormRegion VARCHAR(256),
				FormCity VARCHAR(256),
				FormStreet VARCHAR(256),
				FormHouse VARCHAR(64),
				FormBuilding VARCHAR(128),
				FormOffice VARCHAR(64),
				FormKLADRCode VARCHAR(128),
				FormFIASCode VARCHAR(128),
				FormalizedArea VARCHAR(256),
				FormalizedLocality VARCHAR(256),
				Scale VARCHAR(256),
				TimeZone VARCHAR(24),
				Type TINYINT,
				PRIMARY KEY (ID)
			) $charset_collate",
		];
		require_once( ABSPATH . 'wp-admin/includes/upgrade.php'  );
		foreach ($lists_queries as $sql) {
			dbDelta($sql);
		}
	}

	public function additional_value($price) {
		$additional_value = (int)get_option('iml_additional_value');
		if ($additional_value && $additional_value > 0) {
			$price += $additional_value;
		}
		return $price;
	}
}
