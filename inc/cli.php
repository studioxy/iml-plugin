<?php

function iml_shipping_cli_parse_sd() {
	IMLShippingApi::parseSD();
}
WP_CLI::add_command('iml:parseSD', 'iml_shipping_cli_parse_sd');

function iml_shipping_cli_parse_lists() {
	IMLShippingApi::parseSD();
	IMLShippingApi::parseRegionsList();
	IMLShippingApi::parseServicesList();
	IMLShippingApi::parseOrderStatusesList();
	IMLShippingApi::parseDeliveryStatusesList();
}
WP_CLI::add_command('iml:parse', 'iml_shipping_cli_parse_lists');

function iml_shipping_create_order() {
	$order = wc_get_order(69);
	$response = IMLShippingApi::createOrder($order, true);
	dump($response);
}
WP_CLI::add_command('iml:order', 'iml_shipping_create_order');

function calculate_link() {
	$data = [
		'price' => 0,
		'pvz' => '2000968',
	];
	$type = 'pvz';
	IMLShippingApi::getPrice($data, $type);
}
WP_CLI::add_command('iml:calc', 'calculate_link');
