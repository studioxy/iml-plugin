<?php

class IMLShippingOrder {
	private $iml_order_info;
	public function __construct() {
		add_action('add_meta_boxes', [$this, 'add_metaboxes']);
		add_action('woocommerce_resume_order', [$this, 'woocommerce_new_order'], 10, 1);
	}

	public function woocommerce_new_order($order_id) {
		$order = wc_get_order($order_id);
		IMLShippingApi::createOrder($order);
	}


	public function add_metaboxes() {
		add_meta_box('iml_data', __('Iml order data', 'iml-shipping'), [$this, 'iml_data'], 'shop_order', 'side', 'core');
	}

	public function iml_data($post, $meta) {
		$this->getOrderInfo($post->ID);
		wp_nonce_field('iml-shipping', 'iml_shipping');
		echo '<table>';
		$this->print_check();
		$this->order_status($post->ID);
		$this->parcel_places($post->ID);
		echo '</table>';
	}

	private function getOrderInfo($order_id) {
		$iml_order_info = get_post_meta($order_id, 'iml_order_info', true);

		if (!$iml_order_info) {
			try {
				$iml_order_info_response = IMLShippingApi::getOrderData($order_id);
			} catch (Exception $e) {}
			if ($iml_order_info_response) {
				update_post_meta($order_id, 'iml_order_info', $iml_order_info_response);
				$iml_order_info = $iml_order_info_response;
			} else {
				return false;
			}
		}

		$this->iml_order_info = $iml_order_info[0];
	}

	private function print_check() {
		echo '<tr><th><a href="#">' . __('Print', 'iml-shipping') . '</a></tr>';
	}

	private function order_status($post_id) {
		$order_status =  "{$this->iml_order_info['OrderStatus']} - {$this->iml_order_info['StateDescription']}";
		echo '<tr><th><label for="iml_order_status">' . __('Order status', 'iml-shipping') . '</label></th><td>' . $order_status . '</td></tr>';
	}

	private function parcel_places($post_id) {
		$parcel_places = 1;
		echo '<tr><th><label for="iml_order_status">' . __('Order places', 'iml-shipping') . '</label></th><td><input name="iml_parcel_places" value="' . $parcel_places . '"/></td></tr>';
	}

	private function places($post_id) {
	}
}

new IMLShippingOrder();
