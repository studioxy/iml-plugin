<?php

class IML_Checkout {
	public function __construct() {
		add_filter('woocommerce_checkout_fields', [$this, 'change_city_input_type']);
		add_action('rest_api_init', [$this, 'rest_api_init']);
		add_action('woocommerce_after_shipping_rate', [$this, 'checkout_order_review'], 10, 2);
		add_action('woocommerce_new_order', [$this, 'new_order'], 0);
		add_action('woocommerce_after_order_notes', [$this, 'after_order_notes']);
		add_action('woocommerce_checkout_update_order_review', [$this, 'update_order_review']);
	}

	public function after_order_notes() {
		woocommerce_form_field('pvz_address', [
			'type' => 'hidden',
		]);
	}

	public function rest_api_init() {
		register_rest_route('iml-shipping/v1', 'city-autocomplete', [
			'methods' => 'GET',
			'callback' => [$this, 'city_autocomplete'],
		]);
		register_rest_route('iml-shipping/v1', 'sd-list', [
			'methods' => 'GET',
			'callback' => [$this, 'sd_list'],
		]);
		register_rest_route('iml-shipping/v1/', 'force-update', [
			'methods' => 'GET',
			'callback' => [$this, 'force_update'],
		]);
	}

	public function force_update() {
		$authData = IMLShippingApi::getAuthData();
		if ($authData['http_user']) {
			IMLShippingApi::parseSD();
			IMLShippingApi::parseRegionsList();
			IMLShippingApi::parseServicesList();
			IMLShippingApi::parseOrderStatusesList();
			IMLShippingApi::parseDeliveryStatusesList();
		}
		return true;
	}

	public function city_autocomplete($request) {
		$data =  $request->get_params();
		$regions = IMLShippingApi::getRegionsList($data['q']);
		return $regions;
	}

	public function sd_list($request) {
		$data = $request->get_params();
		$sds = IMLShippingApi::getSDByRegion($data['q']);
		return $sds;
	}

	public function new_order($post_id) {
		$shipping_method = array_shift($_POST['shipping_method']);
        $shipping_method_name = preg_replace('/\d+/', '',  $shipping_method);

		if (substr($shipping_method_name, 0, 7) === 'iml_pvz') {
			update_post_meta($post_id, 'cost_counter', get_option('cost_counter'));
			if (isset($_POST['pvz_address'])) {
				update_post_meta($post_id, 'pvz_address', $_POST['pvz_address']);
			}
		}
	}

	public function checkout_order_review($method, $index) {
		global $woocommerce;
		$shipping_method_name = preg_replace('/\d+/', '',  $method->id);
		$shipping_method_name = str_replace(':', '', $shipping_method_name );
		$shipping = WC_Shipping::instance();
		$shipping->load_shipping_methods();
		$shipping_methods = $shipping->get_shipping_methods();

		$shipping_methods_object = $shipping_methods[$shipping_method_name];

		if (substr($shipping_method_name, 0, 7) === 'iml_pvz' && is_checkout()) {
            $shipping_methods_object->instance_id = str_replace($shipping_method_name.':', '', $method->id );
			$authData = IMLShippingApi::getAuthData();

			$city = '';

			$billing_city = WC()->customer->get_city();
			$shipping_city = WC()->customer->get_shipping_city();

			if (!empty($shipping_city)) {
				$city = $shipping_city;
			} else {
				$city = $billing_city;
			}

			$state = WC()->customer->get_shipping_state();
			$weight = 0;
            $weights = '0';
			$current_unit = strtolower(get_option('woocommerce_weight_unit'));
			$weight_c = 1;

			switch ($current_unit){
				case 'kg':
					$weight_c = 1000;
				break;
			}

			$dimension_c = 1;
            $dimension_unit = strtolower(get_option('woocommerce_dimension_unit'));

            switch($dimension_unit){
                case 'm':
                    $dimension_c = 100;
                    break;
                case 'mm':
                    $dimension_c = 0.1;
                    break;
            }
			$cart_products = $woocommerce->cart->get_cart();
            $countProduct = count($cart_products);

            $height = 0;
            $depth = 0;
            $width = 0;
			foreach($cart_products as $cart_product){
				$product = new WC_Product($cart_product['product_id']);
				$itemWeight = (float)$product->get_weight();
				$itemWeight = $itemWeight * $weight_c;

				if ($countProduct == 1 && ($cart_product['quantity'] == 1)) {
					$height = (float)$product->get_height()*$dimension_c;
					$depth = (float)$product->get_length()*$dimension_c;
					$width = (float)$product->get_width()*$dimension_c;
				}

				$weight += (!empty($itemWeight) ? $itemWeight : (float)$shipping_methods_object->get_option( 'default_weight' ) ) * $cart_product['quantity'];
				$weights .= !empty($itemWeight) ? ','.($itemWeight * $cart_product['quantity']) : ','.((float)$shipping_methods_object->get_option( 'default_weight' ) * $cart_product['quantity']);
			}

			global $wpdb;

			$display = 'display:none;';
			$raw_methods_sql = "SELECT count(zone_id) as count_active FROM {$wpdb->prefix}woocommerce_shipping_zone_methods WHERE is_enabled = 1";
			$result = $wpdb->get_results( $wpdb->prepare( $raw_methods_sql,[])) ;

			if ($result[0]->count_active == '1'){
				$display ='';
			}

			$totalval = $woocommerce->cart->cart_contents_total+$woocommerce->cart->tax_total;
			$surch = $shipping_methods_object->get_option( 'surch')!='' ? (int)$shipping_methods_object->get_option( 'surch') : 1;
			if ($shipping_methods_object->id == 'iml_pvz_after') {
                $payment = $totalval;
            }
            if ($shipping_methods_object->id == 'iml_pvz'){
                $payment = 0;
            }

			$actionButton = "";

			$mapOption = get_option('yandex_maps_api');

			if ($mapOption) {
				$actionButton = '<a href=# id="pvz_select_button"
                   data-method="' . $method->method_id . '"
                   data-pvz-city="' . $city . '"
                   data-pvz-weight="[' . $weights . ']"
                   data-paymentsum="' . $payment . '"
                   data-ordersum="' . $totalval . '"
                   data-height="' . $height . '"
                   data-width="' . $width . '"
                   data-depth="' . $depth . '"
					>' . __('Choose', 'iml-shipping') . '</a>';
			} else {
				$options = '';
				if ($city) {
					$pvzs = IMLShippingApi::getSDList($city);
					if (count($pvzs) > 0) {
						foreach ($pvzs as $pvz) {
							$options .= "<option value='{$pvz['ID']}'>{$pvz['Address']}</option>";
						}
					} else {
						$options .= '<option>' . __('No pvz in your city', 'iml-shipping') . '</option>';
					}

					$actionButton = '<select style="max-width: 200px" name="pvz_address" id="pvz_address"
						data-surch = '.$surch.'
						data-method="' . $method->method_id . '"
						data-pvz-city="' . $city . '"
						data-pvz-weight="[' . $weights . ']"
						data-paymentsum="' . $payment . '"
						data-ordersum="' . $totalval . '"
						data-height="' . $height . '"
						data-width="' . $width . '"
						data-depth="' . $depth . '"
						> ' . $options . '</select>';
				} else {
					$actionButton = __('Please select you city', 'iml-shipping');
				}
			}
			echo '<p class="pvz_select_wrapper">' . $actionButton  . ' </p>';
		}
	}

	public function change_city_input_type($fields) {
		$activated = get_option('city_select_activate');
		if (!empty($activated) && $activated['checked'] === "1") {
			$fields['billing']['billing_city']['type'] = 'select';
			$fields['billing']['billing_city']['options'] = [
				'',
			];
			$fields['billing']['billing_city']['input_class'] = ['select2-city'];
			$fields['shipping']['shipping_city']['type'] = 'select';
			$fields['shipping']['shipping_city']['options'] = [
				'',
			];
		}

		return $fields;
	}


	function update_order_review($data) {
		//dump($data);
		$pvz_address = isset($_POST['pvz_address']) ? $_POST['pvz_address'] : '';
		//dump($_REQUEST);
		if ($pvz_address) {
			WC()->session->set('pvz_address', $pvz_address);
		}
	}
}

new IML_Checkout();
