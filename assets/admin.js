(function ($) {
	$(document).ready(function() {
		$('#force_update').on('click', function () {
			$this = $(this);
			$this.attr('disabled', 'disabled');
			$('#force_update_text').text(imlShipping.i18n.update_wip);
			$.ajax({
				url: imlShipping.rest.forceUpdate,
				method: 'GET',
				success: function (response) {
					//console.log('response', response);
					$this.removeAttr('disabled');
					$('#force_update_text').text(imlShipping.i18n.update_success);
				},
			});
		});
	})
})(jQuery);
