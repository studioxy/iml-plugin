;(function ($) {
	function PVZSelect () {
		$('select#pvz_address').on('change', function () {
			$('input[name=pvz_address_hidden]').val($(this).children('option:selected').val());
			$('body').trigger('update_checkout');
		});

		if (window.imlShipping.keys.yamaps) {
			MicroModal.init();
		}

		$('#pvz_select_button').on('click', function (e) {
			e.preventDefault();
			var map = PVZMap();
			if (map) {
				MicroModal.show('pvz-select-modal');
			}
		});
	}
	function sdPreview() {
	}
	function PVZMap() {
		var city;
		if (window.imlShipping.options.woocommerce_ship_to_destination === 'shipping') {
			city = $('[name="shipping_city"]').val();
		} else {
			city = $('[name="billing_city"]').val();
		}
		console.log('city', city);
		if (!city || city === '0') {
			alert(window.imlShipping.i18n.no_city_selected);
			return false;
		}
		var geocoder = ymaps.geocode(city);
		geocoder.then(function (response) {
			var properies = response.geoObjects.get(0).properties.getAll();
			if (properies.boundedBy.length > 0) {
				center = properies.boundedBy[0];
				var map = new ymaps.Map('pvz_map', {
					center: center,
					zoom: 11,
					controls: [
						'geolocationControl',
						'fullscreenControl',
						'zoomControl',
					],
				});
				$.ajax({
					url: window.imlShipping.rest.sdList,
					method: 'GET',
					data: {
						q: city,
					},
					success: function (response) {
						console.log('sd-list', response);
						if (response.length > 0) {
							for (var i = 0; i < response.length; i++) {
								var lng = response[i].Longitude;
								var ltd = response[i].Latitude;
								if (lng && ltd) {
									var placeMark = new ymaps.Placemark([ltd, lng], response[i], {
										iconImageHref: window.imlShipping.vars.assetsFolder + 'pickpoint.png',
										iconLayout: 'default#image',
										iconImageSize: [31, 41],
									});
									placeMark.events.add('click', function (e) {
										console.log(e);
									})
									map.geoObjects.add(placeMark);
								}
							}
						} else {
							$('#pvz_info .select-title').text(window.imlShipping.i18n.no_sds_in_region);
						}
					}
				})
			}
		});
		return true;
	}
	$(document).ready(function () {
		console.log('initApp')
		PVZSelect();
		$('.select2-city').select2({
			ajax: {
				url: window.imlShipping.rest.cityAutocomplete,
				dataType: 'json',
				delay: 250,
				data: function (params) {
					return {
						q: params.term,
					};
				},
				processResults: function (data, params) {
					var terms = [];
					if (data) {
						$.each(data, function (id, text) {
							terms.push({
								id: text.Code,
								text: text.Description,
							});
						});
					}
					return {
						results: terms,
					}
				},
				minimumInputLength: 1,
			},
		});
		$('body').on('updated_checkout', function () {
			console.log('updated_checkout');
			PVZSelect();
		})
	});
})(jQuery);
