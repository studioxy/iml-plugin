<?php
/**
 * Plugin Name:     Iml Shipping
 * Plugin URI:      PLUGIN SITE HERE
 * Description:     PLUGIN DESCRIPTION HERE
 * Author:          YOUR NAME HERE
 * Author URI:      YOUR SITE HERE
 * Text Domain:     iml-shipping
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Iml_Shipping
 */

require plugin_dir_path(__FILE__) . 'inc/helper.php';
require plugin_dir_path(__FILE__) . 'inc/api.php';
require plugin_dir_path(__FILE__) . 'inc/checkout.php';
require plugin_dir_path(__FILE__) . 'inc/courier-shipping-method.php';
require plugin_dir_path(__FILE__) . 'inc/courier-shipping-method-after.php';
require plugin_dir_path(__FILE__) . 'inc/pvz-shipping-method.php';
require plugin_dir_path(__FILE__) . 'inc/pvz-shipping-method-after.php';
require plugin_dir_path(__FILE__) . 'inc/order.php';

if (defined('WP_CLI') && WP_CLI) {
	require plugin_dir_path(__FILE__) . 'inc/cli.php';
}

class IMLShipping {

	private $version;

	public function __construct() {
		$this->version = '0.0.1';
		register_activation_hook(__FILE__, [$this, 'on_plugin_activate']);
		register_deactivation_hook(__FILE__, [$this, 'on_plugin_deactivate']);
		add_action('admin_init', [$this, 'settings_init']);
		add_action('admin_menu', [$this, 'admin_menu']);
		add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);
		add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_scripts']);
		add_action('wp_footer', [$this, 'pvz_on_map_select_micromodal']);
		add_action('admin_notices', [$this, 'admin_notices']);

		add_action('iml_shipping_cron', [$this, 'update_lists']);
	}

	public function admin_enqueue_scripts() {
		wp_enqueue_script('iml-shipping', plugin_dir_url(__FILE__) . 'assets/admin.js', ['jquery'], $this->version);
		wp_localize_script('iml-shipping', 'imlShipping', [
			'rest' => [
				'forceUpdate' => rest_url('iml-shipping/v1/force-update'),
			],
			'i18n' => [
				'update_wip' => __('Updating lists. Please do not reload page!'),
				'update_success' => __('Updated completed!'),
			],
		]);
	}

	public function on_plugin_activate() {
		IMLHelper::createTables();

		wp_clear_scheduled_hook('iml_shipping_cron');
		wp_schedule_event(time(), 'daily', 'iml_shipping_cron');
	}

	public function on_plugin_deactivate() {
		IMLHelper::deleteTables();
	}

	public function admin_notices() {
		if (IMLHelper::checkAuth() === false) {
			$message = __('IML Shipping plugin requires authentication!');
			echo '<div class="notice notice-error ">
				<p>' . $message . '</p>
			</div>';
		}
	}

	public function admin_menu() {
		add_options_page(__('IML settings', 'iml-shipping'), __('IML settings', 'iml-shipping'), 'manage_options', 'iml-settings', [$this, 'admin_settings_page']);
	}

	public function admin_settings_page() {
		echo '<h1>' . __('IML settings page', 'iml-shipping') . '</h1>';
		echo '<form method="post" action="options.php">';
		settings_fields('iml-settings');
		do_settings_sections('iml-settings');
		echo '<table class="form-table">';

		$user = esc_attr( get_option('iml_http_user')  );
		$password = esc_attr( get_option('iml_http_password')  );

		// iml auth
		echo '<tr valign="top">';
		echo '<th scope="row">' . __('HTTP user', 'iml-shipping') . '</th>';
		echo '<td><input type="text" name="iml_http_user" value="' . $user . '" /></td>';
		echo '</tr>';
		echo '<tr valign="top">';
		echo '<th scope="row">' . __('HTTP password', 'iml-shipping') . '</th>';
		echo '<td><input type="text" name="iml_http_password" value="' . $password . '" /></td>';
		echo '</tr>';

		// yandex maps api
		echo '<tr valign="top">';
		echo '<th scope="row">' . __('Yandex maps api', 'iml-shipping') . '</th>';
		echo '<td><input type="text" name="yandex_maps_api" value="' . esc_attr( get_option('yandex_maps_api')  ) . '" /></td>';
		echo '</tr>';

		if ($user != '' && $password != '') {
			echo '<tr valign="top">';
			echo '<th scope="row">' . __('Is IML test', 'iml-shipping') . '</th>';
			$is_iml_test = get_option('is_iml_test');
			$checked = $is_iml_test['checked'] === "1" ? ' checked="checked" ' : '';
			echo '<td><input type="checkbox" name="is_iml_test[checked]" value="1" ' . $checked . ' /></td>';
			echo '</tr>';

			echo '<tr valign="top">';
			echo '<th scope="row">' . __('City select activate', 'iml-shipping') . '</th>';
			$city_select_activate = get_option('city_select_activate');
			$checked_city_select_activate = $city_select_activate['checked'] === "1" ? ' checked="checked" ' : '';
			echo '<td><input type="checkbox" name="city_select_activate[checked]" value="1" ' . $checked_city_select_activate . ' /></td>';
			echo '</tr>';

			echo '<tr valign="top">';
			echo '<th scrope="row">' . __('Prices calculate', 'iml-shipping') . '</th>';
			$prices_calculate = get_option('cost_counter');
			$available_prices_calculate_options = [
				'api' => __('IML server', 'iml-shipping'),
				'table' => __('Table', 'iml-shipping'),
			];
			echo '<td>';
			foreach ($available_prices_calculate_options as $key => $option) {
				$checked = $prices_calculate === $key ? ' checked="checked" ' : '';
				echo "<label for=\"cost_counter_$key\"><input type=\"radio\" $checked value=\"$key\" name=\"cost_counter\" id='cost_counter_$key'>$option</label>";

			}
			echo '</td>';

			$table_counter = [
				[
					'title' => __('Courier. My region', 'iml-shipping'),
					'name' => 'courier_my_region',
					'value' => get_option('courier_my_region'),
				],
				[
					'title' => __('Courier. Other regions', 'iml-shipping'),
					'name' => 'courier_other_regions',
					'value' => get_option('courier_other_regions'),
					'default' => 300,
				],
				[
					'title' => __('PVZ. My region', 'iml-shipping'),
					'name' => 'pvz_my_region',
					'value' => get_option('pvz_my_region'),
					'default' => 300,
				],
				[
					'title' => __('PVZ. Other regions', 'iml-shipping'),
					'name' => 'pvz_other_regions',
					'value' => get_option('pvz_other_regions'),
					'default' => 300,
				],
				[
					'title' => __('Additional value', 'iml-shipping'),
					'name' => 'iml_additional_value',
					'value' => get_option('iml_additional_value'),
					'default' => 0,
				],
			];
			foreach ($table_counter as $tr) {
				$value = $tr['value'] != '' ? $tr['value'] : $tr['default'];
				echo "<tr><th>{$tr['title']}</th><td><input name=\"{$tr['name']}\" value=\"{$value}\" type=\"number\"></td></tr>";
			}

			$regionsList = IMLShippingApi::getRegionsList();

			if (count($regionsList) > 0) {
				$shop_iml_region = get_option('shop_iml_region');
				$options = '';
				foreach ($regionsList as $region) {
					$selected = $shop_iml_region === $region['Code'] ? ' selected ' : '';
					$options .= "<option value='{$region['Code']}' $selected>{$region['Description']}</option>";
				}
				echo '<th>' . __('Shop region', 'iml-shipping') . '</th>';
				echo '<td><select name="shop_iml_region">' . $options . '</select>';

			}
		} else {
			echo '<p>' . __('Please enter auth credentials first', 'iml-shipping') . '</p>';
		}

		echo '</table>';
		submit_button();
		echo '</form>';

		echo '<br/>';
		echo '<hr/>';
		echo '<p id="force_update_text"></p>';

		echo "<button id=force_update class='button button-primary' type=button>" . __('Force update') . "</button>";
	}

	public function settings_init() {
		add_settings_section(
			'iml_settings_section',
			__('IML settings', 'iml-shipping'),
			[$this, 'iml_settings_section_callback'],
			'iml-settings'
		);
		register_setting('iml-settings', 'iml_http_user');
		register_setting('iml-settings', 'iml_http_password');
		register_setting('iml-settings', 'yandex_maps_api');
		register_setting('iml-settings', 'is_iml_test');
		register_setting('iml-settings', 'cost_counter');
		register_setting('iml-settings', 'courier_my_region');
		register_setting('iml-settings', 'courier_other_regions');
		register_setting('iml-settings', 'pvz_my_region');
		register_setting('iml-settings', 'pvz_other_regions');
		register_setting('iml-settings', 'city_select_activate');
		register_setting('iml-settings', 'shop_iml_region');
		register_setting('iml-settings', 'iml_additional_value');
	}

	public function iml_settings_section_callback() {
	}

	public function enqueue_scripts() {
		if (function_exists('WC') && (is_cart() || is_checkout())) {
			wp_enqueue_style('iml-shipping-micromodal-css', plugin_dir_url(__FILE__) . 'assets/app.css', [], $this->version);
			wp_enqueue_script('iml-shipping', plugin_dir_url(__FILE__) . 'assets/app.js', ['jquery', 'select2'], $this->version);
			$mapOption = get_option('yandex_maps_api');
			wp_localize_script('iml-shipping', 'imlShipping', [
				'rest' => [
					'cityAutocomplete' => get_rest_url(0, 'iml-shipping/v1/city-autocomplete'),
					'sdList' => get_rest_url(0, 'iml-shipping/v1/sd-list'),
				],
				'keys' => [
					'yamaps' => $mapOption,
				],
				'options' => [
					'woocommerce_ship_to_destination' => get_option('woocommerce_ship_to_destination'),
				],
				'i18n' => [
					'no_sds_in_region' => __('No SD in region', 'iml-shipping'),
					'no_city_selected' => __('Please select city first', 'iml-shipping'),
				],
				'vars' => [
					'assetsFolder' => plugin_dir_url(__FILE__) . 'assets/',
				]
			]);

			if ($mapOption) {
				wp_enqueue_script('iml-shipping-micromodal', plugin_dir_url(__FILE__) . 'assets/micromodal.min.js', ['jquery'], $this->version);
				wp_enqueue_script('iml-shipping-yamaps', "https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=$mapOption");
			}
		}
	}

	public function pvz_on_map_select_micromodal() {

		if (function_exists('WC') && (is_cart() || is_checkout())) {

		echo '<div class="modal micromodal-slide" id="pvz-select-modal" aria-hidden="true">
			<div class="modal__overlay" tabindex="-1" data-micromodal-close>
				<div class="modal__container" role="dialog" aria-modal="true" aria-labelledby="modal-1-title">
					<header class="modal__header">
						<h2 class="modal__title" id="modal-1-title">
						' . __('Choose pvz on map', 'iml-shipping') . '</h2>
						<button class="modal__close" aria-label="Close modal" data-micromodal-close></button>
					</header>
			<main class="modal__content" id="modal-1-content">
				<div class="select-row">
					<div class="select-row__item" id="pvz_info">
						<div class="select-title">' . __('Choose point to show info', 'iml-shipping') . '</div>
					</div>
					<div class="select-row__item" id="pvz_map"></div>
				</div>
			</main>
			<footer class="modal__footer">
			</footer>
			</div>
			</div>
			</div>
		';
		}
	}
	public function iml_shipping_shedule() {
		add_action('iml_shipping_cron', [$this, 'update_lists']);
	}

	public function update_lists() {
		$authData = IMLShippingApi::getAuthData();
		if ($authData['http_user']) {
			IMLShippingApi::parseSD();
			IMLShippingApi::parseRegionsList();
			IMLShippingApi::parseServicesList();
			IMLShippingApi::parseOrderStatusesList();
			IMLShippingApi::parseDeliveryStatusesList();
		}
	}
}

new IMLShipping();
